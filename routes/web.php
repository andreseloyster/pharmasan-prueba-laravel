<?php

use App\Http\Livewire\Clientes;
use App\Http\Livewire\Reporte;
use App\Http\Livewire\Usuarios;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('guest');


Route::group(['middleware' => 'auth'], function () {
    Route::middleware(['auth:sanctum'])->get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('clientes', Clientes::class)->name('clientes');
    Route::get('usuarios', Usuarios::class)->name('usuarios')->middleware('admin');
    Route::get('reportes', Reporte::class)->name('reportes');
});
