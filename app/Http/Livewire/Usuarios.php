<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Usuarios extends Component
{
    use WithPagination;

    protected $queryString = [
        'buscar' => ['except' => ''],
        'perPage' => ['except' => '5']
    ];

    public $name, $password, $email, $usuario_id;

    public $isModalOpen = 0;

    public $buscar = "";

    public $perPage = "5";

    protected $listeners = [
        'deleteUsuario'=>'delete'
    ];

    public function render()
    {
        return view('livewire.usuarios', [
            'usuarios' => User::buscar($this->buscar)->paginate($this->perPage)
        ]);
    }
    public function limpiar()
    {
        $this->buscar = "";
        $this->page = 1;
        $this->perPage = "5";
    }
    public function create()
    {
        $this->resetFormulario();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isModalOpen = true;
    }

    public function closeModal()
    {
        $this->resetErrorBag();
        $this->resetValidation();
        $this->isModalOpen = false;
    }
    private function resetFormulario()
    {
        $this->usuario_id = null;
        $this->name = '';
        $this->email = '';
        $this->password = null;
    }

    public function store()
    {
        $rules = [
            'name' => 'required',
            'email' => "required|email|unique:users,email,{$this->usuario_id}"
    
        ];
        if ($this->password) {
            $rules['password'] = ['min:6'];
        }elseif($this->usuario_id == null && $this->password == null){
            $rules['password'] = ['min:6'];
        }
        $data = $this->validate($rules);

        User::updateOrCreate(['id' => $this->usuario_id], $data);

        session()->flash('message', $this->usuario_id ? 'Usuario actualizado.' : 'Usuario creado.');

        $this->closeModal();
        $this->resetFormulario();
    }
    
    public function edit(User $usuario)
    {
        $this->usuario_id = $usuario->id;
        $this->name = $usuario->name;
        $this->email = $usuario->email;

        $this->openModal();
    }

    public function delete(User $usuario)
    {
        $usuario->delete();
        session()->flash('message', 'El usuario ha sido eliminado');
    }
}
