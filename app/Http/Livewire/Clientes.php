<?php

namespace App\Http\Livewire;

use App\Models\Cliente;
use Livewire\Component;
use Livewire\WithPagination;

class Clientes extends Component
{
    use WithPagination;

    protected $queryString = [
        'buscar' => ['except' => ''],
        'perPage' => ['except' => '5']
    ];

    public $name, $document, $address, $email, $cliente_id;

    public $isModalOpen = 0;

    public $buscar = "";

    public $perPage = "5";

    protected $listeners = [
        'deleteCliente'=>'delete'
    ];


    public function render()
    {
        return view('livewire.clientes', [
            'clientes' => Cliente::buscar($this->buscar)->paginate($this->perPage)
        ]);
    }

    public function limpiar()
    {
        $this->buscar = "";
        $this->page = 1;
        $this->perPage = "5";
    }
    public function create()
    {
        $this->resetFormulario();
        $this->openModal();        
    }

    public function openModal()
    {
        $this->isModalOpen = true;
    }

    public function closeModal()
    {
        $this->resetErrorBag();
        $this->resetValidation();
        $this->isModalOpen = false;
    }
    private function resetFormulario()
    {
        $this->name = '';
        $this->email = '';
        $this->document = '';
        $this->address = '';
    }
    protected $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'document' => 'required',
        'address' => 'required'

    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
        $this->validate();

        Cliente::updateOrCreate(['id' => $this->cliente_id], [
            'name' => $this->name,
            'email' => $this->email,
            'address' => $this->address,
            'document' => $this->document
        ]);

        session()->flash('message', $this->cliente_id ? 'Cliente actualizado.' : 'Cliente creado.');

        $this->closeModal();
        $this->resetFormulario();
    }

    public function edit(Cliente $cliente)
    {
        $this->cliente_id = $cliente->id;
        $this->name = $cliente->name;
        $this->email = $cliente->email;
        $this->document = $cliente->document;
        $this->address = $cliente->address;

        $this->openModal();
    }

    public function delete(Cliente $cliente)
    {
        $cliente->delete();
        session()->flash('message', 'El cliente ha sido eliminado');
    }
}
