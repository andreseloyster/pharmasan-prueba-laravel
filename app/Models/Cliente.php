<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Jetstream\HasProfilePhoto;


class Cliente extends Model
{
    use HasFactory;
    use HasProfilePhoto;

    protected $guarded = [];

    protected $appends = [
        'profile_photo_url',
    ];

    public function scopeBuscar($query, $buscar)
    {
        if ($buscar)
            return $query->where('name', 'LIKE', "%$buscar%")
                ->orWhere('document', 'LIKE', "%$buscar%")
                ->orWhere('email', 'LIKE', "%$buscar%")
                ->orWhere('address', 'LIKE', "%$buscar%");
    }
}
