<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Reporte') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <h5 wire:loading>Esto puede tardar varios minutos, por favor espere. Cargando...</h5>
                            <div class="flex bg-white px-4 py-3  border-t border-gray-200 sm:px-6">
                                <button type="button" wire:click="exportar()" wire:loading.attr="disabled"  class="ml-2 mr-2 bg-green-700 text-white font-bold py-2 px-4 rounded my-3">
                                    Exporta Excel
                                </button>
                                @if($buscar !== '')
                                <button wire:click="limpiar" class="mr-2 form-input rounded-md shadow-sm mt-1 ml-6 btn-block">X</button>
                                @endif
                                <input type="text" wire:model="buscar" class="form-input rounded-md shadow-sm mt-1 block w-full text-sm" placeholder="Buscar descripcion...">
                                <div class="mt-1 ml-6">
                                    <select wire:model="perPage" class="form-input rounded-md shadow-sm block outline-none text-gray-500 text-sm">
                                        <option value="5">5 por página</option>
                                        <option value="15">15 por página</option>
                                        <option value="25">25 por página</option>
                                    </select>
                                </div>


                            </div>
                            <table class="min-w-full divide-y divide-gray-200">
                                <thead class="bg-gray-50">
                                    <tr>
                                        <th scope="col" class="px-6 py-3 text-left bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            id
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            codigo
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            descripcion
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            laboratorio
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            invima
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            vigencia
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            fecha1
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            fecha2
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                    @foreach($reportes as $reporte)
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="flex items-center">

                                                <div class="ml-4">
                                                    <div class="text-sm font-medium text-gray-900">
                                                        {{$reporte->id}}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{$reporte->codigo}}</div>

                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{$reporte->descripcion}}</div>

                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{$reporte->laboratorio}}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            <div class="text-sm text-gray-900">{{$reporte->invima}}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            <div class="text-sm text-gray-900">{{$reporte->vigencia}}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            <div class="text-sm text-gray-900">{{$reporte->fecha1}}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            <div class="text-sm text-gray-900">{{$reporte->fecha2}}</div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="bg-white px-4 py-3 border-t border-gray-200 sm:px-6">
                                {{$reportes->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>