<?php

namespace App\Http\Livewire;

use App\Exports\ReporteExport;
use App\Models\Reporte as ModelsReporte;
use Livewire\Component;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;

class Reporte extends Component
{
    use WithPagination;

    protected $queryString = [
        'buscar' => ['except' => ''],
        'perPage' => ['except' => '5']
    ];

    public $buscar = "";

    public $perPage = "5";

    public function render()
    {
        return view('livewire.reporte',[
            'reportes'=>ModelsReporte::where('descripcion','LIKE',"%{$this->buscar}%")->paginate($this->perPage)
        ]);
    }

    public function limpiar()
    {
        $this->buscar = "";
        $this->page = 1;
        $this->perPage = "5";
    }

    public function exportar()
    {
        return Excel::download(new ReporteExport, 'reporte.csv');
    }
}
