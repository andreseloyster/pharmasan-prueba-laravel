@if (session()->has('message'))
<div class="bg-indigo-100 border-t-4 border-indigo-500 rounded-b text-indigo-900 px-4 py-3 shadow-md my-3" role="alert">
    <div class="flex">
        <div>
            <h4 class="text-sm">{{ session('message') }}</h4>
        </div>
    </div>
</div>
@endif